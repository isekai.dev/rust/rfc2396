/// `*pchar`
pub fn param(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize0(crate::parsers::char::p_char)(i)
}

/// `*digit`
pub fn port(i: &str) -> nom::IResult<&str, &str> {
    nom::character::complete::digit0(i)
}

/// `*uric`
pub fn query(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize0(crate::parsers::char::uri_c)(i)
}

/// `1*( unreserved | escaped | "$" | "," | ";" | ":" | "@" | "&" | "=" | "+" )`
pub fn reg_name(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize1(nom::branch::alt((
        crate::parsers::char::unreserved,
        crate::parsers::char::escaped,
        nom::combinator::recognize(nom::character::complete::one_of("$,;:@&=+")),
    )))(i)
}

/// `1*( unreserved | escaped | ";" | "@" | "&" | "=" | "+" | "$" | "," )`
pub fn rel_segment(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize1(nom::branch::alt((
        crate::parsers::char::unreserved,
        crate::parsers::char::escaped,
        nom::combinator::recognize(nom::character::complete::one_of(";@&=+$,")),
    )))(i)
}

/// `*( unreserved | escaped | ";" | ":" | "&" | "=" | "+" | "$" | "," )`
pub fn user_info(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize0(nom::branch::alt((
        crate::parsers::char::unreserved,
        crate::parsers::char::escaped,
        nom::combinator::recognize(nom::character::complete::one_of(";:&=+$,")),
    )))(i)
}

#[cfg(test)]
mod tests;
