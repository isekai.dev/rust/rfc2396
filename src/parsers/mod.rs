pub use uri::uri_reference;

#[cfg(test)]
macro_rules! test_cases {
    (
        $module:ident::$parser:ident: {
            $( $case:ident: $text:literal ),+
        }
    ) => {$(
        #[test]
        fn $case() -> anyhow::Result<()> {
            assert_eq!(crate::parsers::$module::$parser($text).context(concat!("failed to parse `", $text, "`"))?, ("", $text));
            Ok(())
        }
    )+};
}

mod char;
mod component;
mod multi_char;
mod uri;
