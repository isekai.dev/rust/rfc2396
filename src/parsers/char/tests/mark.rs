use anyhow::Context;

test_cases! {
    char::mark: {
        case_apostrophe: "'",
        case_asterisk: "*",
        case_dash: "-",
        case_exclamation: "!",
        case_parenthesis_left: "(",
        case_parenthesis_right: ")",
        case_period: ".",
        case_tilde: "~",
        case_underscore: "_"
    }
}
