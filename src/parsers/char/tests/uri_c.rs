use anyhow::Context;

test_cases! {
    char::uri_c: {
        case_ampersand: "&",
        case_at: "@",
        case_colon: ":",
        case_comma: ",",
        case_dollar: "$",
        case_equals: "=",
        case_plus: "+",
        case_question: "?",
        case_semicolon: ";",
        case_forward_slash: "/",
        case_0: "0",
        case_1: "1",
        case_2: "2",
        case_3: "3",
        case_4: "4",
        case_5: "5",
        case_6: "6",
        case_7: "7",
        case_8: "8",
        case_9: "9",
        case_a: "a",
        case_a_upper: "A",
        case_apostrophe: "'",
        case_asterisk: "*",
        case_b: "b",
        case_b_upper: "B",
        case_c: "c",
        case_c_upper: "C",
        case_d: "d",
        case_d_upper: "D",
        case_dash: "-",
        case_e: "e",
        case_e_upper: "E",
        case_exclamation: "!",
        case_f: "f",
        case_f_upper: "F",
        case_g: "g",
        case_g_upper: "G",
        case_h: "h",
        case_h_upper: "H",
        case_i: "i",
        case_i_upper: "I",
        case_j: "j",
        case_j_upper: "J",
        case_k: "k",
        case_k_upper: "K",
        case_l: "l",
        case_l_upper: "L",
        case_m: "m",
        case_m_upper: "M",
        case_n: "n",
        case_n_upper: "N",
        case_o: "o",
        case_o_upper: "O",
        case_p: "p",
        case_p_upper: "P",
        case_parenthesis_left: "(",
        case_parenthesis_right: ")",
        case_period: ".",
        case_q: "q",
        case_q_upper: "Q",
        case_r: "r",
        case_r_upper: "R",
        case_s: "s",
        case_s_upper: "S",
        case_t: "t",
        case_t_upper: "T",
        case_tilde: "~",
        case_u: "u",
        case_u_upper: "U",
        case_underscore: "_",
        case_v: "v",
        case_v_upper: "V",
        case_w: "w",
        case_w_upper: "W",
        case_x: "x",
        case_x_upper: "X",
        case_y: "y",
        case_y_upper: "Y",
        case_z: "z",
        case_z_upper: "Z",
        case_00: "%00",
        case_02: "%02",
        case_04: "%04",
        case_0b_upper: "%0B",
        case_0e_upper: "%0E",
        case_0f_upper: "%0F",
        case_14: "%14",
        case_15: "%15",
        case_1c: "%1c",
        case_1d: "%1d",
        case_1d_upper: "%1D",
        case_20: "%20",
        case_28: "%28",
        case_29: "%29",
        case_2c_upper: "%2C",
        case_2e: "%2e",
        case_31: "%31",
        case_34: "%34",
        case_47: "%47",
        case_48: "%48",
        case_4d: "%4d",
        case_4e_upper: "%4E",
        case_4f: "%4f",
        case_50: "%50",
        case_57: "%57",
        case_5a: "%5a",
        case_5c: "%5c",
        case_5c_upper: "%5C",
        case_5d_upper: "%5D",
        case_5f: "%5f",
        case_5f_upper: "%5F",
        case_65: "%65",
        case_69: "%69",
        case_6b_upper: "%6B",
        case_6c: "%6c",
        case_6e: "%6e",
        case_72: "%72",
        case_7a: "%7a",
        case_7a_upper: "%7A",
        case_7c: "%7c",
        case_7c_upper: "%7C",
        case_7e: "%7e",
        case_86: "%86",
        case_88: "%88",
        case_8c: "%8c",
        case_8d: "%8d",
        case_91: "%91",
        case_96: "%96",
        case_9a: "%9a",
        case_a0_upper: "%A0",
        case_a2_upper: "%A2",
        case_a4: "%a4",
        case_a5: "%a5",
        case_ab_lower_upper: "%aB",
        case_ad_lower_upper: "%aD",
        case_b2: "%b2",
        case_b4_upper: "%B4",
        case_b9: "%b9",
        case_b9_upper: "%B9",
        case_ba_lower_upper: "%bA",
        case_bb_lower_upper: "%bB",
        case_bb_upper_upper: "%BB",
        case_bc_lower_upper: "%bC",
        case_be: "%be",
        case_be_upper_upper: "%BE",
        case_bf_upper_upper: "%BF",
        case_c5: "%c5",
        case_ca: "%ca",
        case_cb_upper_lower: "%Cb",
        case_cd_lower_upper: "%cD",
        case_cd_upper_lower: "%Cd",
        case_d4_upper: "%D4",
        case_d5_upper: "%D5",
        case_d8_upper: "%D8",
        case_d9: "%d9",
        case_dc: "%dc",
        case_df_upper_lower: "%Df",
        case_e4: "%e4",
        case_e7: "%e7",
        case_ea_upper_lower: "%Ea",
        case_ec_upper_lower: "%Ec",
        case_ef_upper_lower: "%Ef",
        case_ef_upper_upper: "%EF",
        case_f1_upper: "%F1",
        case_fa_upper_lower: "%Fa",
        case_fd_upper_upper: "%FD",
        case_fe: "%fe",
        case_ff_upper_lower: "%Ff"
    }
}
