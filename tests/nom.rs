use anyhow::Context;

macro_rules! test_cases {
    (
        $( $case:ident: $text:literal ),+
    ) => {$(
        #[test]
        fn $case() -> anyhow::Result<()> {
            assert_eq!(rfc2396::validate_nom($text).context(concat!("failed to parse `", $text, "`"))?, ("", $text));
            Ok(())
        }
    )+};
}

include!("test_cases/mod.rs");
