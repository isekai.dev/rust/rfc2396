macro_rules! test_cases {
    (
        $( $case:ident: $text:literal ),+
    ) => {$(
        #[test]
        fn $case() {
            assert!(rfc2396::validate($text));
        }
    )+};
}

include!("test_cases/mod.rs");
