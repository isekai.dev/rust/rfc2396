_Note: not published to https://crates.io; see version 1.0.1_

- Initial "stable" release and publication to [crates.io](https://crates.io/crates/rfc2396)
